public class Main {
    public static void main(String[] args) {
        int j = 1, k = 1;
        System.out.print("for: ");
        for (int i = 0 ; i < 11; i++){
              if (i%2 == 1) {
                  System.out.print(j + " ");
                  j+=k;
              }
              else{
                  System.out.print(k + " ");
                  k+=j;
              }
        }
        System.out.print("\nWhile: ");
        j = 1; k = 1;
        int i = 0;
        while (i < 11){
            if (i%2 == 1) {
                System.out.print(j + " ");
                j+=k;
            }
            else{
                System.out.print(k + " ");
                k+=j;
            }
            i++;
        }
    }
}
